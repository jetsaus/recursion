<?php
/*
 * Функция преобразования строки целых чисел, разделенных пробелами в массив целых чисел integer
 * Если чисел не введено, возвращает пустой массив
 * Символы и цифры с символами считаются символами и отбрасываюся, преобразованию не подлежат, например 'd', '5a', 'f7'
 * @param   string  $strInt                 строка целых чисел, разделенных пробелами
 * @return  array   $arrInt                 массив целых чисел типа integer
 */
function transformStrToInt(
    $strInt = ''                                                            //строка целых чисел, разделенных пробелами
):array                                                                     //массив целых чисел типа integer
{
    $arrInt = [];                                                           //массив целых чисел типа integer
    $arrStr = explode(' ', trim($strInt));
    $arrInt = array_map('intval', array_filter($arrStr, 'is_numeric'));     //преобразование в массив integer
    return $arrInt;
}
function gw($chars, $length, &$words, $prefix = '') {
    if (strlen($prefix) == $length) {
        $words[] = $prefix;
        return;
    }
    for ($i = 0; $i < strlen($chars); $i++) {
        gw($chars, $length, $words, $prefix . $chars[$i]);
    }
    return;
}
function comGivLen(
    $arrInt = [],                           // массив целых уникальных чисел
    $k = 0                                  // количество комбинаций
)
{
    if (strlen($prefix) == $length) {
        $words[] = $prefix;
        return;
    }
    for ($i = 0; $i < strlen($chars); $i++) {
        comGivLen($chars, $length, $words, $prefix . $chars[$i]);
    }
    return;
}